# 奇文相册（模仿QQ相册）

## 介绍

基于springboot框架开发的Web相册。

 **您可以在其基础上继续进行开发来完善其功能，成为本项目的贡献者之一** 

 **您也可以以该项目作为脚手架，进行其他项目的开发** 

## 软件架构
该项目主要采用Spring Boot 2进行开发和部署,主要用到以下关键技术

**前台**：elementui，layui，vue,  swipper

**后台框架**：springboot mybatis jpa


## 安装教程

1. 拉取代码
2. 本地创建数据库，名为album，将application.properties中连接数据库的密码替换为自己本地的
3. 点击根目录下install.bat进行编译
4. 编译完成之后会生成release发布包，进去点击startWeb.bat启动

## 使用说明
启动完成之后可访问localhost:8080进行访问
## 演示
注册相册
![image.png](https://images.gitee.com/uploads/images/2020/0408/232423_2fc7135e_947714.png)
创建相册
![image.png](https://images.gitee.com/uploads/images/2020/0408/232422_04d24a65_947714.png)
创建相册可以添加相册描述，当然创建成功后也支持修改，当添加完描述后，鼠标经过相册会出现遮罩用来显示描述，如下：
![image.png](https://images.gitee.com/uploads/images/2020/0408/232423_b80022d8_947714.png)

进入相册查看图片列表
![image.png](https://images.gitee.com/uploads/images/2020/0408/232423_c72d8ee0_947714.png)

图片内容轮播图查看，可以通过键盘控制切换，并且支持评论和回复哦
![image.png](https://images.gitee.com/uploads/images/2020/0408/232422_aa5d4730_947714.png)

批量操作，批量删除，也可以取消批量操作
![image.png](https://images.gitee.com/uploads/images/2020/0408/232424_3d0dd3de_947714.png)

批量上传，上传的过程中可以显示进度
![image.png](https://images.gitee.com/uploads/images/2020/0408/232423_e71ef9b2_947714.png)


## 联系我
各种问题可扫描加入QQ群进行咨询

**QQ交流群**请扫描下面二维码

<img width="30%" src="https://images.gitee.com/uploads/images/2020/0406/164832_5121dc5e_947714.png"/>

**微信公众号**请扫描下面二维码

<img width="30%" src="https://images.gitee.com/uploads/images/2020/0406/164833_d99e92ee_947714.png"/>

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


## 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
