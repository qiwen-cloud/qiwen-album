package com.qiwenshare.web.domain;

import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.*;

/**
 * 个人分类实体类
 *
 * @author ma116
 */
@Table(name = "personalsort",  uniqueConstraints = {
        @UniqueConstraint(name = "uniquePersonalSort", columnNames = {"personalSortName","userid"})
        })
@Entity
public class PersonalSortBean {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long personalSortId;
    @Column
    private String personalSortName;

    private long userid;

    public long getPersonalSortId() {
        return personalSortId;
    }

    public void setPersonalSortId(long personalSortId) {
        this.personalSortId = personalSortId;
    }

    public String getPersonalSortName() {
        return personalSortName;
    }

    public void setPersonalSortName(String personalSortName) {
        this.personalSortName = personalSortName;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }
}
