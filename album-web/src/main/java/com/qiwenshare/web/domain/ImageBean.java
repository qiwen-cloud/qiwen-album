//package com.qiwenshare.web.domain;
//
//import javax.persistence.*;
//
///**
// * 相册实体类
// *
// * @author ma116
// */
//@Table(name = "image")
//@Entity
//public class ImageBean {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private long imageid;
//    @Column
//    private long albumid;
//    @Column
//    private String imageurl;
//    @Column
//    private String uploadtime;
//
//    private String timestampname;
//
//    private String extendname;
//
//    private String imagename;
//
//    private long imagesize;
//
//    public ImageBean(){
//
//    }
//
//    public ImageBean(long imageid) {
//        this.imageid = imageid;
//    }
//
//    public String getExtendname() {
//        return extendname;
//    }
//
//    public void setExtendname(String extendname) {
//        this.extendname = extendname;
//    }
//
//    public String getTimestampname() {
//        return timestampname;
//    }
//
//    public void setTimestampname(String timestampname) {
//        this.timestampname = timestampname;
//    }
//
//    public String getImagename() {
//        return imagename;
//    }
//
//    public void setImagename(String imagename) {
//        this.imagename = imagename;
//    }
//
//    public long getImagesize() {
//        return imagesize;
//    }
//
//    public void setImagesize(long imagesize) {
//        this.imagesize = imagesize;
//    }
//
//    public long getImageid() {
//        return imageid;
//    }
//
//    public void setImageid(long imageid) {
//        this.imageid = imageid;
//    }
//
//    public long getAlbumid() {
//        return albumid;
//    }
//
//    public void setAlbumid(long albumid) {
//        this.albumid = albumid;
//    }
//
//    public String getImageurl() {
//        return imageurl;
//    }
//
//    public void setImageurl(String imageurl) {
//        this.imageurl = imageurl;
//    }
//
//    public String getUploadtime() {
//        return uploadtime;
//    }
//
//    public void setUploadtime(String uploadtime) {
//        this.uploadtime = uploadtime;
//    }
//
//
//}
