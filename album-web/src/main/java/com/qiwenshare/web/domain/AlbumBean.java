package com.qiwenshare.web.domain;

import javax.persistence.*;
import java.util.List;

/**
 * 相册实体类
 *
 * @author ma116
 */
@Table
@Entity(name = "album")
public class AlbumBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long albumid;
    @Column
    private long userid;
    @Column
    private String albumname;
    @Column
    private String createtime;
    @Column
    private String albumintro;
	@Column
    private String albumpower;

    @Transient
    private List<FileBean> fileBeanList;

    public List<FileBean> getFileBeanList() {
        return fileBeanList;
    }

    public void setFileBeanList(List<FileBean> fileBeanList) {
        this.fileBeanList = fileBeanList;
    }

    public long getAlbumid() {
        return albumid;
    }

    public void setAlbumid(long albumid) {
        this.albumid = albumid;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getAlbumname() {
        return albumname;
    }

    public void setAlbumname(String albumname) {
        this.albumname = albumname;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getAlbumintro() {
        return albumintro;
    }

    public void setAlbumintro(String albumintro) {
        this.albumintro = albumintro;
    }
	
	public String getAlbumpower() {
        return albumpower;
    }

    public void setAlbumpower(String albumpower) {
        this.albumpower = albumpower;
    }
}
