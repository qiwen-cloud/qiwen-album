package com.qiwenshare.web.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.qiwenshare.common.domain.TableData;
import com.qiwenshare.web.api.IOperationLogService;

import com.qiwenshare.web.domain.OperationLogBean;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/log")
public class OperationLogController {

    @Resource
    IOperationLogService operationLogService;

    /**
     * 获取用户的所有相册列表
     *
     * @return
     */
    @RequestMapping(value = "/getoperationloglist", method = RequestMethod.GET)
    @ResponseBody
    public TableData<List<OperationLogBean>> getOperationLogList(HttpServletRequest request) {
        TableData<List<OperationLogBean>> tableData = new TableData<List<OperationLogBean>>();
        List<OperationLogBean> operationLogList = operationLogService.selectOperationLog();
        tableData.setData(operationLogList);
        return tableData;
    }

}
