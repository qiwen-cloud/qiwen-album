package com.qiwenshare.web.service;

import java.util.List;

import javax.annotation.Resource;

import com.qiwenshare.web.api.IOperationLogService;
import com.qiwenshare.web.domain.OperationLogBean;
import com.qiwenshare.web.mapper.OperationLogMapper;
import org.springframework.stereotype.Service;


@Service
public class OperationLogService implements IOperationLogService {

    @Resource
    OperationLogMapper operationLogMapper;

    @Override
    public List<OperationLogBean> selectOperationLog() {
        List<OperationLogBean> result = operationLogMapper.selectOperationLog();
        return result;
    }

    @Override
    public void insertOperationLog(OperationLogBean operationlogBean) {
        operationLogMapper.insertOperationLog(operationlogBean);

    }


}
