package com.qiwenshare.web.service;

import java.util.List;

import javax.annotation.Resource;

import com.qiwenshare.web.api.IAlbumService;
import com.qiwenshare.web.domain.AlbumBean;
import com.qiwenshare.web.domain.UserBean;
import com.qiwenshare.web.mapper.AlbumMapper;
import org.springframework.stereotype.Service;

@Service
public class AlbumService implements IAlbumService {

    @Resource
    AlbumMapper albumMapper;

    @Override
    public List<AlbumBean> getAlbumListByUser(UserBean userBean) {
        List<AlbumBean> albumList = albumMapper.getAlbumListByUser(userBean);
        return albumList;
    }

    @Override
    public AlbumBean selectAlbum(AlbumBean albumBean) {
        return albumMapper.selectAlbum(albumBean);
    }

    @Override
    public int insertAlbum(AlbumBean albumBean) {
        return albumMapper.insertAlbum(albumBean);

    }

    @Override
    public void uploadAlbum(AlbumBean albumBean) {
        albumMapper.uploadAlbum(albumBean);

    }

    @Override
    public void deleteAlbum(AlbumBean albumBean) {
        albumMapper.deleteAlbum(albumBean);

    }

}
