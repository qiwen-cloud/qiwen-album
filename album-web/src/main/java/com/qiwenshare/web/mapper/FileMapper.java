package com.qiwenshare.web.mapper;

import com.qiwenshare.web.domain.*;
import com.qiwenshare.web.domain.AlbumBean;
import com.qiwenshare.web.domain.FileBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FileMapper {

    void insertFile(FileBean fileBean);
    void batchInsertFile(List<FileBean> fileBeanList);
    void updateFile(FileBean fileBean);
    FileBean selectFileById(FileBean fileBean);
    List<FileBean> selectFilePathTreeByUserid(FileBean fileBean);
    FileBean selectDirFileByAlbumid(AlbumBean fileBean);
    List<FileBean> selectFileList(FileBean fileBean);
    List<FileBean> selectFileTreeListLikeFilePath(FileBean fileBean);
    void deleteFileById(FileBean fileBean);
    void deleteFileByIds(List<Integer> fileidList);
    public List<FileBean> selectFileListByIds(List<Integer> fileidList);
    List<FileBean> selectFileByAlbum(AlbumBean albumBean);
    void deleteFileByAlbum(AlbumBean albumBean);

    void updateFilepathByFilepath(String oldfilepath, String newfilepath);
    void updateFilepathByPathAndName(String oldfilepath, String newfilepath, String filename, String extendname);
    List<FileBean> selectFileByExtendName(@Param("filenameList") List<String> filenameList,
                                          @Param("userid") long userid);
}
