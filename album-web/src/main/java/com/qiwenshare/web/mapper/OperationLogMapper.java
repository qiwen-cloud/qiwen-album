package com.qiwenshare.web.mapper;

import com.qiwenshare.web.domain.OperationLogBean;

import java.util.List;

public interface OperationLogMapper {
    List<OperationLogBean> selectOperationLog();

    void insertOperationLog(OperationLogBean operationlogBean);
}
