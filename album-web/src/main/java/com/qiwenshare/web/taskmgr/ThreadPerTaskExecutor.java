package com.qiwenshare.web.taskmgr;

import java.util.concurrent.Executor;

/**
 * 为每个请求启动一个新线程
 *
 * @author ma116
 */
public class ThreadPerTaskExecutor implements Executor {

    @Override
    public void execute(Runnable command) {
        new Thread(command).start();

    }

}
