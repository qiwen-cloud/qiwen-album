package com.qiwenshare.web.api;

import com.qiwenshare.web.domain.AlbumBean;
import com.qiwenshare.web.domain.UserBean;

import java.util.List;

public interface IAlbumService {


    /**
     * 获取用户相册列表
     *
     * @param userBean 用户信息
     * @return 相册列表
     */
    List<AlbumBean> getAlbumListByUser(UserBean userBean);

    /**
     * 获取相册内容
     * @param albumBean 相册信息
     * @return 相册内容
     */
    AlbumBean selectAlbum(AlbumBean albumBean);

    /**
     * 添加相册
     * @param albumBean 相册
     * @return int
     */
    int insertAlbum(AlbumBean albumBean);

    /**
     * 修改相册
     *
     * @param albumBean 相册信息
     */
    void uploadAlbum(AlbumBean albumBean);

    /**
     * 删除相册
     *
     * @param albumBean 相册信息
     */
    void deleteAlbum(AlbumBean albumBean);
}
