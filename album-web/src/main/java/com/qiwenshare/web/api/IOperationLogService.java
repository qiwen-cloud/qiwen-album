package com.qiwenshare.web.api;

import com.qiwenshare.web.domain.OperationLogBean;

import java.util.List;

public interface IOperationLogService {
    List<OperationLogBean> selectOperationLog();

    void insertOperationLog(OperationLogBean operationlogBean);
}
