var fileTreeApp;
$(document).ready(function () {
    fileTreeApp = new Vue({
        el: '#fileTreeId',
        data: {
            fileTree: []
        },
        created: function () {
            this.initFileTree();

        },
        methods: {
            initFileTree : function(){
                initFileTree();
            },
            handleNodeClick:function(data) {
                parent.newfilepath = data.attributes.filepath;
            }

        }
    });
});



function initFileTree(){
    $.ajax({
        url: "/file/getFileTree",

        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {
                fileTreeApp.fileTree = [result.data];

            } else {
                alert("失敗" + result);
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });
}