$(document).ready(function () {
    if (document.body.clientWidth < 768) {
        $(".carousel-inner > .item > img").css("height", "250px");
        $(".carousel .item").css("height", "250px");
        $(".carousel").css("height", "250px");
        $("#myCarousel").css("height", "250px");
        var img_circle = document.getElementsByClassName("img-circle");
        for (var i = 0; i < img_circle.length; i++) {
            img_circle[i].setAttribute("width", "70px");
            img_circle[i].setAttribute("height", "70px");
        }
    } else {
        $(".carousel-inner > .item > img").css("height", "500px");
        $(".carousel .item").css("height", "500px");
        $(".carousel").css("height", "500px");
        $("#myCarousel").css("height", "500px");
        var img_circle = document.getElementsByClassName("img-circle");
        for (var i = 0; i < img_circle.length; i++) {
            img_circle[i].setAttribute("width", "140px");
            img_circle[i].setAttribute("height", "140px");
        }
    }
});
