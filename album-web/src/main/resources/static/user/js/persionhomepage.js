$(document).ready(function () {
    selectTab();
    initAttentionCountInfo();
    initFanCountInfo();
    initUserInfo();
    initBlogList();


});

function deleteEssay(essayId) {
    var essayInfo = {};
    essayInfo["essayId"] = essayId;
    var r = confirm("确定删除？");
    if (r == true) {
        $.ajax({
            data: essayInfo,
            url: "/essay/deleteEssayById",
            dataType: "json",
            type: "POST",
            success: function (result) {
                if (result.success) {
                    alert("删除文章成功！")
                    initBlogList();

                } else {
                    alert(result.errorMessage);
                }
            },
            error: function (result) {
                //alert("系统繁忙!");
            }
        });
    } else {
        console.log("你点击了取消")
    }

}

function initBlogList() {

    var userInfo = {};
    userInfo["userId"] = userId;
    $.ajax({
        data: userInfo,
        url: "/essay/getEssayListByUserId",
        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {
                var userList = result.data;
                var essays = [];

                for (var i = 0; i < userList.length; i++) {
                    var title = userList[i].title;
                    var essayId = userList[i].essayId;

                    var essayObj = {};
                    essayObj.title = title;
                    essayObj.essayId = essayId;
                    essayObj.essayUrl = "/essay/essayContent.html?essayId=" + essayId;
                    essayObj.deleteEssay = "deleteEssay(" + essayId + ")";
                    essayObj.essayEditUrl = "/essay/essayEdit.html?essayId=" + essayId;
                    essays.push(essayObj);

                }
                var app4 = new Vue({
                    el: '#show1',
                    data: {
                        essays: essays
                    }
                })


            } else {
                alert(result.errorMessage);
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });
}

//选择tab事件
function selectTab() {
    document.getElementById("show1").style.display = 'block';
    document.getElementById("show2").style.display = 'none';
    document.getElementById("show3").style.display = 'none';
    document.getElementById("show4").style.display = 'none';
    $('#myTabs a').click(function (e) {
        e.preventDefault();
        if (e.currentTarget.id == "click1") {
            document.getElementById("show1").style.display = 'block';
            document.getElementById("tab1").setAttribute("class", "tabactive");
            document.getElementById("tab2").setAttribute("class", "");
            document.getElementById("tab3").setAttribute("class", "");
            document.getElementById("tab4").setAttribute("class", "");
            document.getElementById("show2").style.display = 'none';
            document.getElementById("show3").style.display = 'none';
            document.getElementById("show4").style.display = 'none';
        }
        if (e.currentTarget.id == "click2") {
            document.getElementById("show1").style.display = 'none';
            document.getElementById("show2").style.display = 'block';
            document.getElementById("show3").style.display = 'none';
            document.getElementById("show4").style.display = 'none';
            document.getElementById("tab1").setAttribute("class", "");
            document.getElementById("tab2").setAttribute("class", "tabactive");
            document.getElementById("tab3").setAttribute("class", "");
            document.getElementById("tab4").setAttribute("class", "");
        }
        if (e.currentTarget.id == "click3") {
            document.getElementById("show1").style.display = 'none';
            document.getElementById("show2").style.display = 'none';
            document.getElementById("show3").style.display = 'block';
            document.getElementById("show4").style.display = 'none';
            document.getElementById("tab1").setAttribute("class", "");
            document.getElementById("tab2").setAttribute("class", "");
            document.getElementById("tab3").setAttribute("class", "tabactive");
            document.getElementById("tab4").setAttribute("class", "");
        }
        if (e.currentTarget.id == "click4") {
            document.getElementById("show1").style.display = 'none';
            document.getElementById("show2").style.display = 'none';
            document.getElementById("show3").style.display = 'none';
            document.getElementById("show4").style.display = 'block';
            document.getElementById("tab1").setAttribute("class", "");
            document.getElementById("tab2").setAttribute("class", "");
            document.getElementById("tab3").setAttribute("class", "");
            document.getElementById("tab4").setAttribute("class", "tabactive");
        }
    });
}

var userId;

//初始化用户信息
function initUserInfo() {
    var thisURL = document.URL;
    var getval = thisURL.split('?')[1];
    userId = getval.split("=")[1];
    var userJson = {};
    userJson["userId"] = userId;

    $.ajax({
        data: userJson,
        url: "/user/getUserInfoById",
        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {
                var province = result.data.addrprovince == undefined ? "" : result.data.addrprovince;
                var city = result.data.addrcity == undefined ? "" : result.data.addrcity;
                var area = result.data.addrarea == undefined ? "" : result.data.addrarea;
                var birthday = result.data.birthday == undefined ? "" : result.data.birthday;
                var sex = result.data.sex == undefined ? "" : result.data.sex;
                var position = result.data.position == undefined ? "" : result.data.position;

                var userImage = result.data.imageBeanList == undefined ? "" : result.data.imageBeanList[result.data.imageBeanList.length - 1].imageurl;
                document.getElementById("usernameId").innerHTML = result.data.username;
                document.getElementById("address").innerHTML = province + " " + city + " " + area;
                document.getElementById("birthday").innerHTML = birthday;
                document.getElementById("sex").innerHTML = sex;
                document.getElementById("position").innerHTML = position;

                document.getElementById("userImageId").setAttribute("src", userImage);


            } else {
                alert("失敗" + result);
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });
}

//初始化关注数量信息
function initAttentionCountInfo() {
    var thisURL = document.URL;
    var getval = thisURL.split('?')[1];
    var userId = getval.split("=")[1];

    var userJson = {};
    userJson["userId"] = userId;
    $.ajax({
        data: userJson,
        url: "/user/getAttentionCountByUserId",
        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {
                document.getElementById("attentionCount").innerHTML = result.data;
                document.getElementById("myAttentionId").innerHTML = result.data;


            } else {
                alert("失敗" + result);
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });
}

//初始化粉丝数量信息
function initFanCountInfo() {
    var thisURL = document.URL;
    var getval = thisURL.split('?')[1];
    var userId = getval.split("=")[1];

    var userJson = {};
    userJson["userId"] = userId;
    $.ajax({
        data: userJson,
        url: "/user/getFanCountByUserId",
        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {
                document.getElementById("fanCount").innerHTML = result.data;
                document.getElementById("myFanId").innerHTML = result.data;

            } else {
                alert("失敗" + result);
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });
}


