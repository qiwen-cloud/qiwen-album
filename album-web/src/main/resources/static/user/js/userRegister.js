//注册用户信息
function registerUserInfo() {
    var registerPhone = document.getElementById("registerPhone").value;
    var registerUserName = document.getElementById("registerUserName").value;
    var registerPassword = document.getElementById("registerPassword").value;

    var userInfo = {};
    userInfo["telephone"] = registerPhone;
    userInfo["username"] = registerUserName;
    userInfo["password"] = registerPassword;

    $.ajax({
        data: userInfo,
        url: "/user/adduser",
        dataType: "json",
        type: "POST",
        success: function (result) {
            if (result.success) {
                alert("注册成功");
                window.location.href = "userLogin";
            } else {
                $("#errorMessageAlertId").text(result.errorMessage);
                $("#errorAlertId").css("display", "block");
            }
        },
        error: function (result) {
            //alert("系统繁忙!");
        }
    });
}